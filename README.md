# MessengerData
V2 of my messenger chat analysis program.
Download your information at www.facebook.com/dyi in json format.

The Gson library is required. https://search.maven.org/artifact/com.google.code.gson/gson/2.8.5/jar
This can be downloaded either as a jar or simply by using maven (easy with IntelliJ).

Make more modules to analyse the data in different ways. Request to join then send me a message.

Current plans:
Words per message
Bulk file loading
GUI
Interactive charts
