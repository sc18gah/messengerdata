package GUI;

import IO.Input;
import IO.Output;
import Types.MessagesHour;
import Types.MessagesPerson;
import Types.MessagesWeek;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Collections;
import java.util.List;

import static Modules.Utilities.*;


public class GUIMain extends Application {
    @Override
    public void start(Stage stage) throws NullPointerException {
        stage.setTitle("Messenger Data");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open JSON File");
        File file = fileChooser.showOpenDialog(stage);

        if (file ==  null) {
            new Alert(Alert.AlertType.ERROR, "Cannot Find File. Program Terminated.").showAndWait();
            Platform.exit();
        }

        // Check file extension is JSON
        if (!(file.getName().substring(file.getName().lastIndexOf('.') + 1).equals("json"))) {
            new Alert(Alert.AlertType.ERROR, "Incorrect File Format. Program Terminated.").showAndWait();
            Platform.exit();
        }

        // Read and write data, as normal
        Input input = new Input(file.getAbsolutePath());
        JsonObject baseObj = input.getJsonObject();
        Output output = new Output(baseObj);
        List<MessagesPerson> messagesPersonList = initMessagesPerson(baseObj);
        List<MessagesHour> messagesHourList = initMessagesHour();
        List<MessagesWeek> messagesWeekList = initMessagesWeek(baseObj);
        JsonArray msgArray = baseObj.getAsJsonArray("messages");
        runPerson(output, messagesPersonList, msgArray);
        runHour(output, messagesHourList, msgArray);
        runWeek(output, messagesWeekList, msgArray);
        output.close();

        BarChart<String, Number> personChart = createLinePlotPerson(messagesPersonList, "totalMessages", "Person with Most Messages", "Person", "Message Count");
        int widest = (int)personChart.getMinWidth();
        BarChart<String, Number> hourChart = createLinePlotHour(messagesHourList, "Most Popular Hours", "Hour", "Messages", widest);
        BarChart<String, Number> weekChart = createLinePlotWeek(messagesWeekList, 0, "Most Popular Weeks", "Weeks", "Messages", widest);

        FlowPane root = new FlowPane();
        root.getChildren().addAll(hourChart, weekChart, personChart);

        ScrollPane sp = new ScrollPane(root);
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        Scene scene = new Scene(sp, widest+30, 800);

        stage.setTitle("Messenger Data");
        stage.setScene(scene);
        stage.show();
    }

    private BarChart<String, Number> createLinePlotPerson(List<MessagesPerson> list, String toPlot, String chartTitle, String xAxisLabel, String yAxisLabel) {
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();

        yAxis.setLabel(yAxisLabel);
        BarChart<String, Number> lc = new BarChart<>(xAxis, yAxis);

        lc.setTitle(chartTitle);
        XYChart.Series<String, Number> series = new BarChart.Series<>();
        series.setName(xAxisLabel);

        // Sort list
        if (toPlot.equals("totalMessages")) {
            Collections.sort(list, (t1, t0) -> t0.getTotalMessages() - (t1.getTotalMessages()));
        }

        lc.setMinWidth(0.0);

        // Populate
        for (int i = 0; i < list.size(); i++) {
            if (toPlot.equals("totalMessages")) {
                XYChart.Data<String, Number> data = new BarChart.Data<>(list.get(i).getName(), list.get(i).getTotalMessages());
                series.getData().add(data);
                lc.setMinWidth(lc.getMinWidth()+30.0);
            }
        }

        lc.getData().add(series);
        return lc;
    }

    private BarChart<String, Number> createLinePlotHour(List<MessagesHour> list, String chartTitle, String xAxisLabel, String yAxisLabel, int whole_width) {
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel(xAxisLabel);
        yAxis.setLabel(yAxisLabel);
        BarChart<String, Number> lc = new BarChart<>(xAxis, yAxis);

        lc.setTitle(chartTitle);
        XYChart.Series<String, Number> series = new BarChart.Series<>();
        series.setName(xAxisLabel);


        // Populate
        for (int i = 0; i < list.size(); i++) {
            XYChart.Data<String, Number> data = new BarChart.Data<>(String.format("%d", list.get(i).getHour()), list.get(i).getMessages());
            series.getData().add(data);
        }

        lc.setMinWidth(whole_width/2);
        lc.getData().add(series);
        return lc;
    }

    private BarChart<String, Number> createLinePlotWeek(List<MessagesWeek> list, int dateFormat, String chartTitle, String xAxisLabel, String yAxisLabel, int whole_width) {
        CategoryAxis xAxis = new CategoryAxis();
        NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel(xAxisLabel);
        yAxis.setLabel(yAxisLabel);
        BarChart<String, Number> lc = new BarChart<>(xAxis, yAxis);

        lc.setTitle(chartTitle);
        XYChart.Series<String, Number> series = new BarChart.Series<>();
        series.setName(xAxisLabel);


        // Populate
        for (int i = 0; i < list.size(); i++) {
            XYChart.Data<String, Number> data;
            if (dateFormat == 0) {
                data = new BarChart.Data<>(String.format("%d", list.get(i).getWeekNum()), list.get(i).getMessages());

            } else {
                data = new BarChart.Data<>(list.get(i).getWeekText(), list.get(i).getMessages());
            }
            series.getData().add(data);
        }

        lc.setMinWidth(whole_width/2);
        lc.getData().add(series);
        return lc;
    }
}
