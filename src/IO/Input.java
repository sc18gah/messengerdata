package IO;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Input {
    public JsonObject getJsonObject() {
        return jsonObject;
    }

    private JsonObject jsonObject;

    public Input(String path){
        startInput(path);
    }

    public Input(){
        System.out.println("Enter file path(base is prog dir) and name(excl. json): ");
        Scanner scanner=new Scanner(System.in);
        startInput(scanner.nextLine()+".json");
    }

    private void startInput(String path){
        createJson(loadFile(path));
    }

    private BufferedReader loadFile(String path){
        File file = new File(path);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return br;
    }
    private void createJson(BufferedReader br){
        Gson gson=new Gson();
        jsonObject=gson.fromJson(br,JsonObject.class);
    }
}
