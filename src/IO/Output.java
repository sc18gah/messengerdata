package IO;

import com.google.gson.JsonObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Output {
    private BufferedWriter writer;

    public Output(JsonObject baseObj){
        String saveName=baseObj.get("title").getAsString();
        writer=null;
        try {
            writer=new BufferedWriter(new FileWriter("result_"+saveName+".csv"));
            writer.write("Chat name:,"+saveName+"\n\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeLine(String line){
        try {
            writer.append(line+"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeBlankLines(int numLines){
        for(int i=0;i<numLines;i++){
            writeLine("");
        }
    }
}
