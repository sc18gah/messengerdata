import IO.*;
import Modules.*;
import Types.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import static Modules.Utilities.*;

public class MessengerData {
    public static void main(String[] args){
        Input input=new Input();
        JsonObject baseObj=input.getJsonObject();
        Output output=new Output(baseObj);
        List<MessagesPerson> messagesPersonList=initMessagesPerson(baseObj);
        List<MessagesHour> messagesHourList=initMessagesHour();
        List<MessagesWeek> messagesWeekList=initMessagesWeek(baseObj);
        JsonArray msgArray=baseObj.getAsJsonArray("messages");
        runPerson(output,messagesPersonList,msgArray);
        runHour(output,messagesHourList,msgArray);
        runWeek(output,messagesWeekList,msgArray);
        output.close();
    }
}
