package Modules;

import Types.MessagesHour;
import com.google.gson.JsonArray;

import java.util.Calendar;
import java.util.List;

public class Hour_Total {
    public Hour_Total(List<MessagesHour> list, JsonArray msgArray){
        for(int i=0;i<msgArray.size();i++){
            long ms=msgArray.get(i).getAsJsonObject().get("timestamp_ms").getAsLong();
            Calendar calendar=Calendar.getInstance();
            calendar.setTimeInMillis(ms);
            int hour=calendar.get(Calendar.HOUR_OF_DAY);
            list.get(hour).incMessages();
        }
    }
}
