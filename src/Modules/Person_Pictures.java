package Modules;

import Types.MessagesPerson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public class Person_Pictures {
    public Person_Pictures(List<MessagesPerson> list, JsonArray msgArray){
        for(int i=0;i<msgArray.size();i++){
            JsonObject object=msgArray.get(i).getAsJsonObject();
            for(int x=0;x<list.size();x++){
                if(object.get("sender_name").getAsString().equals(list.get(x).getName())){
                    if(object.has("photos")){
                        list.get(x).incPictureMessages();
                    }
                }
            }
        }
    }
}
