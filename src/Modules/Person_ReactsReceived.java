package Modules;

import Types.MessagesPerson;
import com.google.gson.JsonArray;

import java.util.List;

public class Person_ReactsReceived {
    public Person_ReactsReceived(List<MessagesPerson> list, JsonArray msgArray){
        for(int i=0;i<msgArray.size();i++){
            String name=msgArray.get(i).getAsJsonObject().get("sender_name").getAsString();
            if(msgArray.get(i).getAsJsonObject().has("reactions")){
                JsonArray reaArray=msgArray.get(i).getAsJsonObject().get("reactions").getAsJsonArray();
                for(int x=0;x<list.size();x++){
                    if(name.equals(list.get(x).getName())){
                        for(int y=0;y<reaArray.size();y++){
                            list.get(x).incReactsReceived();
                        }
                    }
                }
            }
        }
    }
}
