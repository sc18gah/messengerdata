package Modules;

import Types.MessagesPerson;
import com.google.gson.JsonArray;

import java.util.List;

public class Person_ReactsSent {
    public Person_ReactsSent(List<MessagesPerson> list, JsonArray msgArray){
        for(int i=0;i<msgArray.size();i++){
            if(msgArray.get(i).getAsJsonObject().has("reactions")){
                JsonArray reaArray=msgArray.get(i).getAsJsonObject().get("reactions").getAsJsonArray();
                for(int x=0;x<reaArray.size();x++){
                    String actor=reaArray.get(x).getAsJsonObject().get("actor").getAsString();
                    for(int y=0;y<list.size();y++){
                        if(actor.equals(list.get(y).getName())){
                            list.get(y).incReactsSent();
                            y=list.size();
                        }
                    }
                }
            }
        }
    }
}
