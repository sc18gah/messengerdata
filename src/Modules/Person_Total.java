package Modules;

import Types.MessagesPerson;
import com.google.gson.JsonArray;

import java.util.List;

public class Person_Total {
    public Person_Total(List<MessagesPerson> list, JsonArray msgArray){
        for (int i=0;i<msgArray.size();i++){
            String name=msgArray.get(i).getAsJsonObject().get("sender_name").getAsString();
            for(int x=0;x<list.size();x++){
                if(name.equals(list.get(x).getName())){
                    list.get(x).incTotalMessages();
                }
            }
        }
    }
}
