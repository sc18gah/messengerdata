package Modules;

import IO.Output;
import Types.MessagesHour;
import Types.MessagesPerson;
import Types.MessagesWeek;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class Utilities {
    public static List<MessagesPerson> initMessagesPerson(JsonObject baseObj){
    List<MessagesPerson> list=new ArrayList<>();
    JsonArray jNamesArray=baseObj.getAsJsonArray("participants");
    for(JsonElement jsonElement:jNamesArray){
        list.add(new MessagesPerson(jsonElement.getAsJsonObject().get("name").getAsString()));
    }
    return list;
}

    public static List<MessagesHour> initMessagesHour(){
        List<MessagesHour> list=new ArrayList<>();
        for(int i=0;i<24;i++){
            list.add(new MessagesHour(i));
        }
        return list;
    }

    public static List<MessagesWeek> initMessagesWeek(JsonObject baseObj){
        List<MessagesWeek> list=new ArrayList<>();
        JsonArray jMessagesArray=baseObj.getAsJsonArray("messages");
        long endTime=jMessagesArray.get(0).getAsJsonObject().get("timestamp_ms").getAsLong();
        long startTime=jMessagesArray.get(jMessagesArray.size()-1).getAsJsonObject().get("timestamp_ms").getAsLong();
        long weeks=(endTime-startTime)/604800000+1;
        for(int i=0;i<weeks;i++){
            list.add(new MessagesWeek(i,startTime));
        }
        return list;
    }

    public static void runPerson(Output output, List<MessagesPerson> messagesPersonList, JsonArray msgArray){
        new Person_Total(messagesPersonList,msgArray);
        new Person_Pictures(messagesPersonList,msgArray);
        new Person_ReactsReceived(messagesPersonList,msgArray);
        new Person_ReactsSent(messagesPersonList,msgArray);

        output.writeLine(messagesPersonList.get(0).getHeader());
        for(MessagesPerson messagesPerson:messagesPersonList){
            output.writeLine(messagesPerson.toString());
        }
        output.writeBlankLines(5);
    }

    public static void runHour(Output output, List<MessagesHour> messagesHourList, JsonArray msgArray){
        new Hour_Total(messagesHourList,msgArray);

        output.writeLine(messagesHourList.get(0).getHeader());
        for(MessagesHour messagesHour:messagesHourList){
            output.writeLine(messagesHour.toString());
        }
        output.writeBlankLines(5);
    }

    public static void runWeek(Output output, List<MessagesWeek> messagesWeekList, JsonArray msgArray){
        new Week_Total(messagesWeekList,msgArray);

        output.writeLine(messagesWeekList.get(0).getHeader());
        for(MessagesWeek messagesWeek:messagesWeekList){
            output.writeLine(messagesWeek.toString());
        }
        output.writeBlankLines(5);
    }
}
