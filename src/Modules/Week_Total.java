package Modules;

import Types.MessagesWeek;
import com.google.gson.JsonArray;

import java.util.List;

public class Week_Total {
    public Week_Total(List<MessagesWeek> list, JsonArray msgArray){
        long startTime=msgArray.get(msgArray.size()-1).getAsJsonObject().get("timestamp_ms").getAsLong();
        for(int i=0;i<msgArray.size();i++){
           int week=(int)((msgArray.get(i).getAsJsonObject().get("timestamp_ms").getAsLong()-startTime)/604800000);
           list.get(week).incMessages();
        }
    }
}
