package Types;

public class MessagesHour {
    private static String header="Hour,Message count";
    private int hour;
    private int messages;

    public MessagesHour(int hour) {
        this.hour=hour;
    }


    public void incMessages(){
        messages++;
    }

    public String getHeader(){
        return header;
    }
    @Override
    public String toString() {
        return  hour+","+
                messages;
    }

    public int getHour() {
        return hour;
    }

    public int getMessages() {
        return messages;
    }
}
