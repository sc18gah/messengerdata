package Types;

public class MessagesPerson{
    private static String header="Name,Total message count,Picture message count,Reacts received,Reacts sent";
    private String name;
    private int totalMessages;
    private int pictureMessages;
    private int reactsReceived;
    private int reactsSent;

    public int getTotalMessages() {
        return totalMessages;
    }

    public int getPictureMessages() {
        return pictureMessages;
    }

    public int getReactsReceived() {
        return reactsReceived;
    }

    public int getReactsSent() {
        return reactsSent;
    }

    public MessagesPerson(String name) {
        this.name = name;
    }

    public void incTotalMessages() {
        this.totalMessages++;
    }

    public void incPictureMessages() {
        this.pictureMessages++;
    }

    public void incReactsReceived() {
        this.reactsReceived++;
    }

    public void incReactsSent() {
        this.reactsSent++;
    }

    public String getName(){
        return name;
    }

    public String getHeader(){
        return header;
    }
    @Override
    public String toString() {
        return  name+","+
                totalMessages+","+
                pictureMessages+","+
                reactsReceived+","+
                reactsSent;
    }
}
