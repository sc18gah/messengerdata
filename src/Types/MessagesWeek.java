package Types;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MessagesWeek {
    private static String header="Week number,Week beginning,Message count";
    private int weekNum;
    private String weekText;
    private int messages;

    public MessagesWeek(int weekNum,long startTime) {
        this.weekNum=weekNum;
        weekToText(startTime);
    }

    private void weekToText(long startTime){
        Date date = new Date(startTime+(weekNum*(long)604800000));
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        weekText=sdf.format(date);
    }

    public void incMessages(){
        messages++;
    }

    public String getHeader(){
        return header;
    }
    @Override
    public String toString() {
        return  (weekNum+1)+","+
                weekText+","+
                messages;
    }

    public int getWeekNum() {
        return weekNum;
    }

    public String getWeekText() {
        return weekText;
    }

    public int getMessages() {
        return messages;
    }
}
